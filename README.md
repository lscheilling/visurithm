# Visurithm

## Class Algorithm

### function initialise()
> Prepares the algorithm to be run

### function step()
> Performs 1 cycle of the algorithm

### var openSet
> Contains all Tiles that still need to be searched

### var closedSet
> Contains all Tiles that have been searched fully

### var path
> Contains the confirmed path result

### var running
> Used to test if the algorithm is running or not

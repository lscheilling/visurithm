import { Tile } from "../tileMap.js";
import AStarClosestStop from "./ClosestStop.js";

export default class AStar {
    static name = "A*";
    static description = "A* is a pathfinding algorithm that extends Dijkstra's algorithm, it explores neighbors in order of travel distance plus the minimal possible distance to the target, leading to a preference to head in the direction of the target."

    initialised = false
    openSet = []
    closedSet = []
    path = []
    target = null;
    finished = false
    offset = 0;

    constructor(controller) {
        this.controller = controller;
        this.map = controller.map;
    }

    initialise() {
        this.initialised = true
        this.currentNode = new Tile(this.map.origin,{travel_cost:0})
        this.openSet.push(this.currentNode)
        this.currentNode.onPath = true;
        this.stops = this.map.stops();
        this.endNode = new Tile(this.map.target);
        this.setTarget()
    }

    run(repeat = true) {
        if (!this.initialised) this.initialise()
        if (this.finished || (!this.controller.settings.isRunning && repeat)) return;
        this.step();
        if (repeat) {
            if (this.controller.settings.speedLimited) setTimeout(this.run.bind(this),1000/this.controller.settings.speed);
            else this.run();
        }
    }

    setTarget() {
        if (this.stops.length == 0) {
            var stopFinder = new AStarClosestStop(this.map, this.currentNode.element,[this.endNode.element]);
            if (stopFinder.findClosestStopIndex() == null) {
                console.log(`SEARCH COMPLETE: NO PATH`)
                this.controller.completeSearch(-1)
                return this.finished = -1
            }
            this.target = this.endNode;
        } else {
            var stopFinder = new AStarClosestStop(this.map, this.currentNode.element,this.stops);
            var index = stopFinder.findClosestStopIndex();
            if (index == null) {
                console.log(`SEARCH COMPLETE: NO PATH`)
                this.controller.completeSearch(-1)
                return this.finished = -1
            }
            this.target = new Tile(this.stops.splice(index,1)[0])
        }
        this.map.clearDataValues();
        this.currentNode.score = this.map.distance(this.currentNode.element, this.target.element);
        this.currentNode.travel_cost = 0;
        this.currentNode.onPath = true
    }

    setTilePath(path, value) {
        if (value) path.forEach((tile,index,arr) => tile.onPath = "#"+this.map.pathGradient.getColour(index / (arr.length-1)))
        else path.forEach(tile => tile.closedSet = true)
    }

    removePath(tile) {
        this.setTilePath(this.getPath(tile), false)
    }

    drawPath(tile = false) {
        this.setTilePath([...this.path,...(tile?this.getPath(tile):[])],true)
    }

    estimate_future_cost(element) {
        return this.map.distance(element, this.target.element)
    }

    getPath(tile) {
        var path = [tile]
        while (path[0].previous) path.splice(0,0,path[0].previous);
        return path
    }

    step() {
        if (this.openSet.length == 0) {
            console.log(`SEARCH COMPLETE: NO PATH`)
            this.controller.completeSearch(-1)
            return this.finished = -1
        };

        // Select best f val
        this.removePath(this.currentNode);
        this.currentNode.closedSet = true
        this.currentNode = this.openSet.shift();
        this.closedSet.push(this.currentNode);
        this.drawPath(this.currentNode)

        // Did I finish?
        if (this.currentNode.element === this.target.element) {
            var path = [this.currentNode]
            while (path[0].previous) path.splice(0,0,path[0].previous);
            if (this.path.length > 0) path = path.slice(1)
            path.forEach(p => p.path_cost = this.offset + p.travel_cost)
            this.path = [...this.path, ...path]
            this.offset = this.path[this.path.length-1].path_cost
            if (this.currentNode.element == this.endNode.element) {
                console.log(`SEARCH COMPLETE [${this.path.toString()}]`)
                console.log(this.path)
                this.controller.completeSearch(1)
                return this.finished = this.path;
            } else {
                this.setTarget()
                this.openSet = [this.currentNode];
                this.closedSet = [];
                this.currentNode.previous = null;
            }
            return;
        } 

        // Check all the neighbors
        var neighbors = this.map.neighbors(this.currentNode.element);
        for (var neighbourGroup of neighbors) {
            var [dist,neighbour] = neighbourGroup;

            if (this.closedSet.find(tile => tile.tileID == `${neighbour.getAttribute("row")},${neighbour.getAttribute("col")}`)) continue; // neighbour already checked so skip

            const existing = this.openSet.findIndex(tile => tile.tileID == `${neighbour.getAttribute("row")},${neighbour.getAttribute("col")}`)
            if (existing >= 0 && existing.score > neighbour.score) this.openSet.splice(existing, 1, this.createNeighbourTile(neighbour, dist))

            else if (existing == -1) this.sortedInsert(this.openSet, this.createNeighbourTile(neighbour, dist));
        }
    }

    createNeighbourTile(neighbour, dist) {
        neighbour = new Tile(neighbour, {
            travel_cost: this.currentNode.travel_cost + dist,
            previous: this.currentNode,
            estimated_future_cost: this.estimate_future_cost(neighbour),
            openSet: true
        })
        neighbour.score = neighbour.travel_cost + neighbour.estimated_future_cost
        return neighbour
    }

    sortedInsert(tiles, tile) {
        var low = 0, high = tiles.length;

        while (low < high) {
            var mid = (low + high) >>> 1;
            if (tiles[mid].score < tile.score || (tiles[mid].score == tile.score && tiles[mid].travel_cost < tile.travel_cost)) low = mid + 1;
            else high = mid;
        }
        tiles.splice(low, 0, tile)
    }
}
class Tile {
    travel = 0
    previous = null
    future = 0
    score = 0
    constructor (element) {
        this.element = element
    }
}

export default class AStarClosestStop {
    closedSet = []
    target = null;
    finished = false

    constructor(map, origin, stops) {
        this.map = map;
        this.origin = origin
        this.stops = stops
    }

    findClosestStop() {
        return this.stops[this.findClosestStopIndex()];
    }

    findClosestStopIndex() {
        var costs = []
        for (const [i, stop] of this.stops.entries()) {
            this.currentNode = new Tile(this.origin)
            this.openSet = [this.currentNode]
            this.closedSet = []
            this.target = new Tile(stop)
            while (!this.finished) this.step()
            if (this.finished == -1) return null
            costs.push([this.finished,i])
            this.finished = false;
        }
        return costs.reduce((prev, curr) => prev[0] < curr[0]?prev:curr,[Infinity,0])[1]
    }

    step() {
        if (this.openSet.length == 0) return this.finished = -1 // No Path

        // Select best f val
        this.currentNode = this.openSet.shift();
        this.closedSet.push(this.currentNode);

        // Did I finish?
        if (this.currentNode.element === this.target.element) return this.finished = this.currentNode.travel;

        // Check all the neighbors
        var neighbors = this.map.neighbors(this.currentNode.element);
        for (var neighbourGroup of neighbors) {
            var [dist,neighbour] = neighbourGroup;
            if (this.closedSet.find(tile => tile.element == neighbour)) continue; // neighbour already checked so skip
            var travel_cost = this.currentNode.travel + dist
            var existing = this.openSet.find(tile => tile.element == neighbour)
            if (existing && existing.travel > travel_cost) {
                existing.travel = travel_cost
                existing.previous = this.currentNode
                existing.score = travel_cost + existing.future
            } else if (!existing) {
                var tile = new Tile(neighbour)
                tile.travel = travel_cost
                tile.future = this.map.distance(neighbour, this.target.element)
                tile.previous = this.currentNode
                tile.score = tile.travel + tile.future
            
                var index = this.openSet.findIndex(t => t.score > tile.score || (t.score == tile.score && t.travel > tile.travel))
                if (index == -1) this.openSet.push(tile)
                else this.openSet.splice(index,0,tile)
            }
        }
    }
}
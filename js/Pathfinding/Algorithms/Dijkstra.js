import AStar from "./AStar.js";

export default class Dijkstra extends AStar  {
    static name = "Dijkstra";
    static description = "Dijkstra is a pathfinding algorithm that explores it's neighbouring nodes in order of the nodes with the least distance from origin."

    estimate_future_cost(element) {
        return 0
    }
}
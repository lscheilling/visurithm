import AStar from "./Algorithms/AStar.js";
import Dijkstra from "./Algorithms/Dijkstra.js";
import HTML from "../htmlBuilder.js";

export default class Settings {
    static diagonalWallSettingsButtonText = ["Diagonal Walls: Disabled","Diagonal Walls: Enabled"];
    static diagonalWallSettingsButtonClass = ["btn-danger","btn-success"];

    static dataSettingsButtonText = ["Show Data: Disabled","Show Data: Enabled"];
    static dataSettingsButtonClass = ["btn-danger","btn-success"];

    static pathDataSettingsButtonText = ["Show Path Distances: Disabled","Show Path Distances: Enabled"];
    static pathDataSettingsButtonClass = ["btn-danger","btn-success"];

    static diagonalMoveSettingsButtonText = ["Diagonal Moves: Disabled","Diagonal Moves: Enabled"];
    static diagonalMoveSettingsButtonClass = ["btn-danger","btn-success"];

    static speedLimitedSettingsButtonText = ["Speed: Full","Speed: Limited"];
    static speedLimitedSettingsButtonClass = ["btn-success","btn-warning"];

    static speedSelectSettingsInputText = "Speed:";
    static speedSelectSettingsInputRange = ["1","100"];

    static tileSizeSelectSettingsInputText = "Map Tile Size: ";
    static tileSizeSelectSettingsInputRange = ["15","60"];
    tileSizeModalElement = HTML.modal("Tile Size Action",HTML.createElement("p",{},{},"The tile size had been changed, would you like to resize the tile map (",HTML.createElement("span",{},{class:"text-danger"},"not doing so will cause unexpected behaviour with touch inputs"),")?"),"Resize","Cancel",{primaryHandler:(()=>{this.controller.map.createMap()}),primaryColour:"success",staticBackdrop:true});
    tileSizeModal = new bootstrap.Modal(this.tileSizeModalElement)

    static weightSelectSettingsInputText = "Weight:";
    static weightSelectSettingsInputRange = ["1","50"];

    static themeSelectSettingsInputText = "Theme:";
    static themeSelectSettingsInputOptions = ["Default", "Arctic", "Sahara"];

    static randomizerSelectSettingsInputText = "Randomizer:";
    static randomizerSelectSettingsInputOptions = ["True Random"];

    static algorithmSelectSettingsInputText = "Algorithm:";
    static algorithmSelectSettingsInputOptions = [AStar,Dijkstra];

    static runSettingsButtonText = ["RUN (R)","STOP (R)"];
    static runSettingsButtonClass = ["btn-success","btn-danger"];

    shortcuts = {
        "r":this.runSettingsButtonPressed.bind(this),
    }

    constructor(controller, algorithm = "A*", diagonalWalls = true, diagonalMoves = true, speedLimited = true, speed = 75, theme="Default", randomizer="True Random", isRunning = false, weightModifier=5, tileSize=25, showData=false, showPathData=false) {
        this.controller = controller;
        document.body.appendChild(this.tileSizeModalElement);

        var [algorithm, algorithmSelectSettingsInput] = this.createDropdownSelect(this.algorithmSelectSettingsInputReceived.bind(this), algorithm, Settings.algorithmSelectSettingsInputText, Settings.algorithmSelectSettingsInputOptions.map(alg => alg.name));
        this.algorithmSelectSettingsInput = algorithmSelectSettingsInput;
        this.algorithmInfo = HTML.createElement("span",{},{class:"mt-auto"},"This is some algorithm info");
        this.algorithmSelectSettingsInputReceived();

        this.diagonalWalls = !diagonalWalls;
        this.diagonalWallSettingsButton = this.createToggleButton(this.diagonalWallSettingsButtonPressed.bind(this),diagonalWalls,Settings.diagonalWallSettingsButtonText,Settings.diagonalWallSettingsButtonClass);

        this.diagonalMoves = diagonalMoves;
        this.diagonalMoveSettingsButton = this.createToggleButton(this.diagonalMoveSettingsButtonPressed.bind(this),diagonalMoves,Settings.diagonalMoveSettingsButtonText,Settings.diagonalMoveSettingsButtonClass);

        this.showData = showData;
        this.dataSettingsButton = this.createToggleButton(this.dataSettingsButtonPressed.bind(this),showData,Settings.dataSettingsButtonText,Settings.dataSettingsButtonClass);

        this.showPathData = showPathData;
        this.pathDataSettingsButton = this.createToggleButton(this.pathDataSettingsButtonPressed.bind(this),showPathData,Settings.pathDataSettingsButtonText,Settings.pathDataSettingsButtonClass);

        this.speedLimited = speedLimited;
        this.speedLimitedSettingsButton = this.createToggleButton(this.speedLimitedSettingsButtonPressed.bind(this),speedLimited,Settings.speedLimitedSettingsButtonText,Settings.speedLimitedSettingsButtonClass);

        var uid = HTML.uid();
        var separator = Math.min(~~(Settings.speedSelectSettingsInputText.length / 3), 11);
        this.speed = Math.max(Math.min(speed,Settings.speedSelectSettingsInputRange[1]),Settings.speedSelectSettingsInputRange[0]);
        this.speedSelectSettingsInput = HTML.row({},
            [separator,HTML.createElement("label",{},{class:"col-form-label",for:uid},Settings.speedSelectSettingsInputText)],
            [12 - separator,HTML.createElement("input",{"onInput":this.speedSelectSettingsInputReceived.bind(this)},{type:"number",class:"form-control", id:uid,value:this.speed.toString(),min:Settings.speedSelectSettingsInputRange[0],max:Settings.speedSelectSettingsInputRange[1],required:"true"})]    
        )

        var uid = HTML.uid();
        var separator = Math.min(~~(Settings.weightSelectSettingsInputText.length / 3), 11);
        this.weightModifier = Math.max(Math.min(weightModifier,Settings.weightSelectSettingsInputRange[1]),Settings.weightSelectSettingsInputRange[0]);
        this.weightSelectSettingsInput = HTML.row({},
            [separator,HTML.createElement("label",{},{class:"col-form-label",for:uid},Settings.weightSelectSettingsInputText)],
            [12 - separator,HTML.createElement("input",{"onInput":this.weightSelectSettingsInputReceived.bind(this)},{type:"number",class:"form-control", id:uid,value:this.weightModifier.toString(),min:Settings.weightSelectSettingsInputRange[0],max:Settings.weightSelectSettingsInputRange[1],required:"true"})]    
        )

        var uid = HTML.uid();
        var separator = Math.min(~~(Settings.tileSizeSelectSettingsInputText.length / 3), 11);
        this.tileSize = Math.max(Math.min(tileSize,Settings.tileSizeSelectSettingsInputRange[1]),Settings.tileSizeSelectSettingsInputRange[0]);
        this.tileSizeSelectSettingsInput = HTML.row({},
            [separator,HTML.createElement("label",{},{class:"col-form-label",for:uid},Settings.tileSizeSelectSettingsInputText)],
            [12 - separator,HTML.createElement("input",{"focusLost":this.tileSizeSelectSettingsInputReceived.bind(this)},{type:"number",class:"form-control", id:uid,value:this.tileSize.toString(),min:Settings.tileSizeSelectSettingsInputRange[0],max:Settings.tileSizeSelectSettingsInputRange[1],required:"true"})]    
        )

        var [theme, themeSelectSettingsInput] = this.createDropdownSelect(this.themeSelectSettingsInputReceived.bind(this), theme, Settings.themeSelectSettingsInputText, Settings.themeSelectSettingsInputOptions);
        this.theme = theme;
        this.themeSelectSettingsInput = themeSelectSettingsInput;

        var [randomizer, randomizerSelectSettingsInput] = this.createDropdownSelect(this.randomizerSelectSettingsInputReceived.bind(this), randomizer, Settings.randomizerSelectSettingsInputText, Settings.randomizerSelectSettingsInputOptions);
        this.randomizer = randomizer;
        this.randomizerSelectSettingsInput = randomizerSelectSettingsInput;

        this.isRunning = isRunning;
        this.runSettingsButton = this.createToggleButton(this.runSettingsButtonPressed.bind(this),isRunning,Settings.runSettingsButtonText,Settings.runSettingsButtonClass,true);
    
        document.addEventListener("keypress", this.keyPressHandler.bind(this));
    }

    postProcessing() {
        this.diagonalWallSettingsButtonPressed();
        this.themeSelectSettingsInputReceived();
    }

    keyPressHandler(event) {
        var key = event.key.toLowerCase();
        if (key in this.shortcuts) this.shortcuts[key]();
    }

    createDropdownSelect(handler, variable, text, options) {
        var uid = HTML.uid();
        var separator = Math.min(~~(text.length / 2.5), 11);
        var selected = options.includes(variable)?variable:options[0];
        var element = HTML.row({},
            [separator,HTML.createElement("label",{},{class:"col-form-label",for:uid},text)],
            [12-separator,HTML.createElement("select",{"onInput":handler},{class:"form-select", id:uid},
                ...options.map((opt) => HTML.createElement("option",{},{value:opt,...(selected == opt?{"selected":"true"}:{})},opt))
            )]    
        )
        return [selected, element]
    }

    dataSettingsButtonPressed() {
        this.showData = this.toggleButton(this.dataSettingsButton, this.showData, Settings.dataSettingsButtonText, Settings.dataSettingsButtonClass);
        this.controller.map.section.classList.toggle("show-data",this.showData)
        console.info(`PATHFINDER SETTINGS: TOGGLED DATA [${this.showData}]`)
    }

    pathDataSettingsButtonPressed() {
        this.showPathData = this.toggleButton(this.pathDataSettingsButton, this.showPathData, Settings.pathDataSettingsButtonText, Settings.pathDataSettingsButtonClass);
        this.controller.map.section.classList.toggle("show-distance",this.showPathData)
        console.info(`PATHFINDER SETTINGS: TOGGLED PATH DISTANCES [${this.showPathData}]`)
    }

    createToggleButton(handler, variable, texts, classes, dismissPanel=false) {
        return HTML.createElement("button",{"onClick":handler},{type:"button",class:"btn btn-transition "+classes[variable?1:0], ...(dismissPanel?{"data-bs-dismiss":"offcanvas"}:{})},texts[variable?1:0])
    }

    toggleButton(button, variable, texts, classes) {
        if (variable) {
            button.innerText = texts[0];
            button.classList.replace(classes[1],classes[0])
        } else {
            button.innerText = texts[1];
            button.classList.replace(classes[0],classes[1])
        }
        return !variable
    }

    diagonalWallSettingsButtonPressed() {
        this.diagonalWalls = this.toggleButton(this.diagonalWallSettingsButton, this.diagonalWalls, Settings.diagonalWallSettingsButtonText, Settings.diagonalWallSettingsButtonClass);
        this.controller.map.section.classList.toggle("diagonals",this.diagonalWalls)
        console.info(`PATHFINDER SETTINGS: TOGGLED DIAGONAL WALLS [${this.diagonalWalls}]`)
    }

    diagonalMoveSettingsButtonPressed() {
        this.diagonalMoves = this.toggleButton(this.diagonalMoveSettingsButton, this.diagonalMoves, Settings.diagonalMoveSettingsButtonText, Settings.diagonalMoveSettingsButtonClass);
        console.info(`PATHFINDER SETTINGS: TOGGLED DIAGONAL MOVES [${this.diagonalMoves}]`)
    }

    runSettingsButtonPressed() {
        this.isRunning = this.toggleButton(this.runSettingsButton, this.isRunning, Settings.runSettingsButtonText, Settings.runSettingsButtonClass);
        console.info(`PATHFINDER SETTINGS: TOGGLED RUN STATUS [${this.isRunning}]`)
        this.algorithmSelectSettingsInput.getElementsByTagName("select")[0].toggleAttribute("disabled",this.isRunning)
        this.diagonalMoveSettingsButton.toggleAttribute("disabled",this.isRunning)
        this.diagonalWallSettingsButton.toggleAttribute("disabled",this.isRunning)
        this.diagonalMoveSettingsButton.toggleAttribute("disabled",this.isRunning)
        this.weightSelectSettingsInput.getElementsByTagName("input")[0].toggleAttribute("disabled",this.isRunning)
        this.tileSizeSelectSettingsInput.getElementsByTagName("input")[0].toggleAttribute("disabled",this.isRunning)
        this.controller.actions.randomizeActionButton.toggleAttribute("disabled",this.isRunning)
        this.controller.actions.clearActionButton.toggleAttribute("disabled",this.isRunning)
        if (this.isRunning) {
            if (this.controller.map.changed) {
                this.createAlgorithmInstance();
                this.controller.map.clearDataValues();
                this.controller.map.changed = false;
            }
            this.algorithmInstance.run()
        }
    }

    speedLimitedSettingsButtonPressed() {
        this.speedLimited = this.toggleButton(this.speedLimitedSettingsButton, this.speedLimited, Settings.speedLimitedSettingsButtonText, Settings.speedLimitedSettingsButtonClass);
        this.speedLimited?this.speedSelectSettingsInput.classList.remove("d-none"):this.speedSelectSettingsInput.classList.add("d-none");
        console.info(`PATHFINDER SETTINGS: TOGGLED SPEED LIMITER [${this.speedLimited}]`)
    }

    speedSelectSettingsInputReceived() {
        var input = this.speedSelectSettingsInput.getElementsByTagName("input")[0];
        if (input.validity.valid) {
            input.classList.remove("is-invalid");
            input.classList.add("is-valid");
            this.speed = input.valueAsNumber;
            console.info(`PATHFINDER SETTINGS: SPEED SET [${this.speed}]`)
        } else {
            input.classList.add("is-invalid");
            input.classList.remove("is-valid");
        }
    }

    weightSelectSettingsInputReceived() {
        var input = this.weightSelectSettingsInput.getElementsByTagName("input")[0];
        if (input.validity.valid) {
            input.classList.remove("is-invalid");
            input.classList.add("is-valid");
            this.weightModifier = input.valueAsNumber;
            console.info(`PATHFINDER SETTINGS: WEIGHT SET [${this.weightModifier}]`)
        } else {
            input.classList.add("is-invalid");
            input.classList.remove("is-valid");
        }
    }

    tileSizeSelectSettingsInputReceived() {
        var input = this.tileSizeSelectSettingsInput.getElementsByTagName("input")[0];
        if (input.validity.valid) {
            input.classList.remove("is-invalid");
            input.classList.add("is-valid");
            this.tileSize = input.valueAsNumber;
            console.info(`PATHFINDER SETTINGS: TILE SIZE SET [${this.tileSize}]`)
            this.tileSizeModal.show();
        } else {
            input.classList.add("is-invalid");
            input.classList.remove("is-valid");
        }
    }

    themeSelectSettingsInputReceived() {
        var select = this.themeSelectSettingsInput.getElementsByTagName("select")[0];
        this.theme = select.value;
        console.info(`PATHFINDER SETTINGS: THEME SET [${this.theme}]`);
        this.controller.map.section.classList.remove(...Settings.themeSelectSettingsInputOptions.map(e => e.toLowerCase()))
        this.controller.map.section.classList.add(this.theme.toLowerCase())
    }

    randomizerSelectSettingsInputReceived() {
        var select = this.randomizerSelectSettingsInput.getElementsByTagName("select")[0];
        this.randomizer = select.value;
        console.info(`PATHFINDER SETTINGS: RANDOMIZER SET [${this.randomizer}]`);
    }

    algorithmSelectSettingsInputReceived() {
        var select = this.algorithmSelectSettingsInput.getElementsByTagName("select")[0];
        var name = select.value;
        this.algorithm = Settings.algorithmSelectSettingsInputOptions.find(algo => algo.name == name);
        this.algorithmInfo.innerText = this.algorithm.description;
        this.createAlgorithmInstance();
        console.info(`PATHFINDER SETTINGS: ALGORITHM SET [${this.algorithm.name}]`);
    }

    createAlgorithmInstance() {
        this.algorithmInstance = new this.algorithm(this.controller);
    }
}
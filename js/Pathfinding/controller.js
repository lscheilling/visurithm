import Actions from "./actions.js";
import Settings from "./settings.js";
import HTML from "../htmlBuilder.js";
import TileMap from "./tileMap.js";

/** Main interface for webpage, used as holder for other components with minimal logic */
class Controller {
    // Load with given parameters within the URL
    static params = new URL(window.location.href).searchParams;

    // Used if algorithm finishes with no result
    static failFindToastHandler = HTML.basicToast("No path exists to stops / target","danger")

    /** Initialises all components of page */
    constructor() {
        this.settings = new Settings(this,Controller.params.get("agr"));
        this.actions = new Actions(this);

        this.navElement = this.createNav();
        document.body.appendChild(this.navElement) // temporarily add to document to be able to calculate scroll height (full height including padding, margin and borders)

        this.map = new TileMap(this, [this.navElement.scrollHeight+5,5,5,5])
        this.settings.postProcessing(); // initialise buttons which need access to map
    }

    /**
     * Create and place buttons on navbar within two separate offcanvas elements
     * @returns {HTMLElement}
     */
    createNav() {
        return HTML.navbar(HTML.navTitle("Visurithm","/"),{},
            HTML.navItemOffcanvas("Settings",HTML.offcanvasTitle("Settings"),{},
                // Algorithm Selection
                this.settings.algorithmSelectSettingsInput,

                this.settings.tileSizeSelectSettingsInput,

                // Diagonal Button
                this.settings.diagonalWallSettingsButton,
                this.settings.diagonalMoveSettingsButton,

                // Speed Input
                this.settings.speedLimitedSettingsButton,
                this.settings.speedSelectSettingsInput,
                this.settings.weightSelectSettingsInput,
                
                // Theme Selection
                this.settings.themeSelectSettingsInput,

                // Randomization Input
                this.settings.randomizerSelectSettingsInput,
                this.actions.randomizeActionButton,

                // Clear Board Button
                this.actions.clearActionButton,

                // Show Data Button
                this.settings.dataSettingsButton,
                this.settings.pathDataSettingsButton,

                // Algorithm Info
                this.settings.algorithmInfo,
            ),
            HTML.navItemOffcanvas("Tools",HTML.offcanvasTitle("Tools"),{},
                // Build Tools
                this.actions.originActionButton,
                this.actions.targetActionButton,
                this.actions.wallActionButton,
                this.actions.stopActionButton,
                this.actions.weightActionButton,

                // Run Tools
                this.actions.stepActionButton,
                this.settings.runSettingsButton,

                // Help
                HTML.createElement("span",{},{class:"mt-auto"},Actions.toolsHelp)
            )
        )
    }

    /**
     * Called by pathfinding algorithm to indicate that a search has completed, stops running and checks for algorithm failure
     * @param {number} code Algorithm finish code (-1 indicates no path was found, otherwise path length)
     */
    completeSearch(code) {
        if (code == -1) Controller.failFindToastHandler()
        if (this.settings.isRunning) this.settings.runSettingsButtonPressed();
        this.map.changed = true // Toggle changed attribute so user can search again
    }
}

/** Entrypoint into the webpage loader */
function setup() {
    var controller = new Controller()
    document.body.replaceChildren( // loading spinner will be replaced at this point
        controller.navElement,
        controller.map.section,
        HTML.toastWrapper,
        HTML.footer("Lucas Spencer-Scheilling")
    )
}

document.addEventListener('DOMContentLoaded', setup)
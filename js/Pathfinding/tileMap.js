import { ColourGradient } from "../utilities.js";
import HTML from "../htmlBuilder.js";

export default class TileMap {
    static toolClasses = ["origin","target","wall","stop","weight"];
    static additionalToolClasses = ["fas fa-running","fas fa-flag-checkered","wall","fas fa-cookie-bite","fas fa-dumbbell"];

    building = false

    tool = 2
    changed = true

    pathGradient = new ColourGradient([0,"ffc10782"],[1,"90ee90ff"])

    prevHeight = window.innerHeight;
    prevWidth = window.innerWidth;
    addressBar = false

    constructor(controller, padding = [5, 5, 5, 5]) {
        this.controller = controller
        this.section = HTML.createElement("section",{},{id:"draw",style:"width:calc(100vw - scrollbar-width);height:100vh;padding:"+padding.map(x => x.toString()+"px").join(" ")})
        document.body.appendChild(this.section)

        var confirmModal = HTML.modal("Resize Action",HTML.createElement("p",{},{},"The window had been resized, would you like to resize the tile map (",HTML.createElement("span",{},{class:"text-danger"},"doing this will erase any map data"),")?"),"Resize","Cancel",{primaryHandler:this.createMap.bind(this),primaryColour:"success",staticBackdrop:true});
        this.confirmModal = new bootstrap.Modal(confirmModal)
        document.body.appendChild(confirmModal);

        if (window.addEventListener) {  // all browsers except IE before version 9
            window.addEventListener ("resize", this.resizeEvent.bind(this), true);
        } else if (window.attachEvent) {   // IE before version 9
            window.attachEvent("onresize", this.resizeEvent.bind(this));
        }
        this.createMap();
    }

    resizeEvent() {
        const [currHeight, currWidth] = [window.innerHeight,window.innerWidth]
        if ((1- (currHeight / this.prevHeight) > 0.35 || Math.abs(currHeight - this.prevHeight) < 75) && Math.abs(1- (currWidth / this.prevWidth)) < 0.01) {
            for (const devices of [/Android/i,/webOS/i,/iPhone/i,/iPad/i,/iPod/i,/BlackBerry/i,/Windows Phone/i]) {
                if (navigator.userAgent.match(devices)) {
                    // no longer needed
                    /*if(Math.abs(currHeight - this.prevHeight) < 75 && !this.addressBar) {
                        this.section.style.paddingBottom = `${parseInt(this.section.style.paddingBottom) + Math.abs(currHeight - this.prevHeight)}px`
                        this.addressBar = true
                        this.confirmModal.show()
                    }*/
                    return;
                }
            }
        }
        [this.prevHeight,this.prevWidth] = [currHeight, currWidth]
        this.confirmModal.show();
    }

    distance(a, b) {
        const [ra, rb, ca, cb] = [parseInt(a.getAttribute("row")),parseInt(b.getAttribute("row")),parseInt(a.getAttribute("col")),parseInt(b.getAttribute("col"))];
        if (this.controller.settings.diagonalMoves) return (Math.min(Math.abs(ra-rb), Math.abs(ca-cb))*Math.sqrt(2)) + Math.abs(Math.abs(ra-rb) - Math.abs(ca-cb))
        return Math.abs(ra-rb) + Math.abs(ca-cb)
    }

    neighbors(a) {
        const [row, col] = [parseInt(a.getAttribute("row")),parseInt(a.getAttribute("col"))];
        if (!this.checkIndex(row, col)) {
            console.warn(`Tile [${row},${col}] does not exist -- probable cause due to resizing of screen`)
            return [];
        }
        const weightParam = this.controller.settings.weightModifier
        const isWeight = this.cells[row][col].classList.contains("weight")
        var neighbors = []
        for (var i=-1; i<=1; i++) for (var j=-1; j<=1; j++) {
            if (!this.checkIndex(row+i, col+j) || (i==0 && j==0) || this.cells[row+i][col+j].classList.contains("wall")) continue
            const nbWeight = this.cells[row+i][col+j].classList.contains("weight")
            if (i!=0 && j!=0) {if (this.controller.settings.diagonalMoves && (!this.controller.settings.diagonalWalls || !this.cells[row+(i==1?1:0)][col+(i==1?0:j)].classList.contains("wall") || !this.cells[row+(i==1?1:0)][col+(i==1?0:j)].classList.contains("wall-above-"+(i==j?"right":"left")))) neighbors.push([(isWeight?weightParam/Math.sqrt(2):1/Math.sqrt(2)) + (nbWeight?weightParam/Math.sqrt(2):1/Math.sqrt(2)),this.cells[row+i][col+j]])} // diagonal check
            else neighbors.push([(isWeight?weightParam*.5:.5) + (nbWeight?weightParam*.5:.5),this.cells[row+i][col+j]])
        }
        return neighbors
    }

    checkIndex(row, col) {
        return row >= 0 && row < this.cells.length && col >= 0 && col < this.cells[0].length
    }

    randomize(method) {
        this.createMap()
        switch (method) {
            case "True Random":
                this.trueRandom();
                break;
            default:
                this.trueRandom();
        }
    }

    trueRandom() {
        var toolWeights = [0.1, 0.1, 30, 0.2, 10]
        toolWeights = toolWeights.map((p) => p/100).map((p,i,w) => p + w.slice(0,i).reduce((a,b)=>a+b,0))
        this.cells.forEach((tableRow,row) => tableRow.forEach((_,col)=> {
            var num = Math.random()
            var tool = toolWeights.findIndex((v) => v > num)
            if (tool >= 0) this.setTile(row, col, tool)
        }))
    }

    createMap() {
        const tileSize = this.controller.settings.tileSize
        const height = this.section.clientHeight - parseFloat(this.section.style.paddingBottom) - parseFloat(this.section.style.paddingTop);
        const width = this.section.clientWidth - parseFloat(this.section.style.paddingLeft) - parseFloat(this.section.style.paddingRight);
        const rows = Math.floor(height / tileSize)
        const cols = Math.floor(width / tileSize)
        const fontSize = Math.max(Math.floor(tileSize / 3) - 2,8)
        if (this.controller.settings.isRunning) this.controller.settings.runSettingsButtonPressed();
        this.cells = []
        this.table = HTML.createElement("table",{},{style:`transition: all .15s; table-layout: fixed;`,class:"h-100 w-100 m-0 position-relative rounded"},
            ...Array.from(Array(rows).keys()).map(thisRow => {
                this.cells.push([])
                return HTML.createElement("tr",{},{rowspan:"1"},
                    ...Array.from(Array(cols).keys()).map(thisCol => {
                        var element = HTML.createElement("td",{"mouseOver":() => this.mouseOver(thisRow, thisCol)},{colspan:"1",row:thisRow.toString(),col:thisCol.toString(),class:"d-table-cell position-relative"}, 
                            HTML.createElement("div",{},{class:"diag-wall"}),
                            HTML.createElement("div",{},{class:"data",style:`font-size:${fontSize}px;line-height:${fontSize}px`})
                        )    
                        this.cells[this.cells.length-1].push(element)
                        return element
                    })
                )
            })
        )
        this.tableHolder = HTML.createElement("div",{},{style:`width:${cols*tileSize}px; max-width:100%; height:${rows*tileSize}px; max-height:100%;`, class:"m-auto"},this.table)
        this.origin = this.cells[Math.floor(rows / 2)][2]
        this.origin.classList.add(TileMap.toolClasses[0],...TileMap.additionalToolClasses[0].split(" "))

        this.target = this.cells[Math.floor(rows / 2)][cols-3]
        this.target.classList.add(TileMap.toolClasses[1],...TileMap.additionalToolClasses[1].split(" "))

        this.section.replaceChildren(this.tableHolder);
        this.tableHolder.addEventListener("touchstart",this.touchStart.bind(this))
        this.tableHolder.addEventListener("touchmove",this.touchMove.bind(this))
        window.addEventListener("touchend",this.touchEnd.bind(this));
        window.addEventListener("mouseup",this.mouseUp.bind(this));
        this.tableHolder.addEventListener("mousedown",this.mouseDown.bind(this));
        this.table.addEventListener("dragstart", (event) => {event.preventDefault()});

        this.changed = true;
    }

    posToCoordinate(pageX, pageY) {
        const left = pageX - this.tableHolder.offsetLeft;
        const top = pageY - this.tableHolder.offsetTop;
        const col = Math.floor(left / this.cells[0][0].clientWidth);
        const row = Math.floor(top / this.cells[0][0].clientHeight);
        return [row, col]
    }

    /* DRAW EVENTS */

    clearClicks() {
        for (var row of this.cells) for (var cell of row) cell.setAttribute("clicked",0)
    }

    touchStart(event) {
        const cells = this.getCellsFromTouches(event.touches);
        // If touched table, start building
        if (cells.length > 0) {
            document.body.style.overflow = "hidden";
            this.building = true;
        }
    }

    touchMove(event) {
        // If previously pressed on table, build where touches are on table
        if (this.building) for (const cell of this.getCellsFromTouches(event.touches)) this.cellBuild(...cell);
    }

    touchEnd(_) {
        // End building
        this.building = false;
        this.clearClicks();
        document.body.style.overflow = "unset";
    }

    getCellsFromTouches(touches) {
        var cells = []
        for (var touch of touches) {
            const [row, col] = this.posToCoordinate(touch.pageX, touch.pageY);
            if (this.checkIndex(row,col)) cells.push([row, col]);
        }
        return cells
    }

    mouseDown(event) {
        const [row, col] = this.posToCoordinate(event.pageX, event.pageY)
        // If touched table, start building
        if (this.checkIndex(row,col)) {
            document.body.style.overflow = "hidden";
            this.building = true;
            this.cellBuild(row,col);
        }
    }

    mouseOver(row, col) {
        // This event is called on each cell, so build without need for check
        if (this.building) this.cellBuild(row,col)
    }

    mouseUp(_) {
        // End building
        this.building = false;
        this.clearClicks();
        document.body.style.overflow = "unset";
    }

    cellBuild(row, col) {
        if (parseInt(this.cells[row][col].getAttribute("clicked"))) return;
        this.cells[row][col].setAttribute("clicked",1)
        this.setTile(row, col, this.tool)
    }

    clearDataValues() {
        for (var row of this.cells) for (var cell of row) new Tile(cell).clear()
    }

    setTile(row, col, tool) {
        this.changed = true
        if (this.controller.settings.isRunning && !(this.controller.settings.algorithm.allowTool && this.controller.settings.algorithm.allowTool.includes(tool))) return;
        switch (tool) {
            case 0:
                this.setOrigin(row, col)
                break;
            case 1:
                this.setTarget(row, col)
                break;
            case 2:
                this.toggleWall(row, col)
                break;
            case 3:
                this.toggleStop(row, col)
                break;
            case 4:
                this.toggleWeight(row, col)
                break;
        }
    }

    stops() {
        return this.cells.flat().filter(cell => cell.classList.contains("stop"));
    }

    removeToolClasses(row, col, tool) {
        var cell = this.cells[row][col]
        if (tool != 2 && cell.classList.contains(TileMap.toolClasses[2])) this.toggleWall(row, col);
        cell.classList.remove(...TileMap.toolClasses.slice(0,tool),...TileMap.toolClasses.slice(tool+1),...TileMap.additionalToolClasses.slice(0,tool).map(e => e.split(" ")).flat(),...TileMap.additionalToolClasses.slice(tool+1).map(e => e.split(" ")).flat())
    }

    setOrigin(row, col) {
        if (this.cells[row][col].classList.contains(TileMap.toolClasses[1])) return;
        this.removeToolClasses(row, col, 0)

        this.origin.classList.remove(TileMap.toolClasses[0],...TileMap.additionalToolClasses[0].split(" "))
        this.origin = this.cells[row][col]
        this.origin.classList.add(TileMap.toolClasses[0],...TileMap.additionalToolClasses[0].split(" "))
    }

    setTarget(row, col) {
        if (this.cells[row][col].classList.contains(TileMap.toolClasses[0])) return;
        this.removeToolClasses(row, col, 1)

        this.target.classList.remove(TileMap.toolClasses[1],...TileMap.additionalToolClasses[1].split(" "))
        this.target = this.cells[row][col]
        this.target.classList.add(TileMap.toolClasses[1],...TileMap.additionalToolClasses[1].split(" "))
    }

    toggleWall(row, col) {
        if (this.cells[row][col].classList.contains(TileMap.toolClasses[0]) || this.cells[row][col].classList.contains(TileMap.toolClasses[1])) return;
        this.removeToolClasses(row, col, 2)

        var cell = this.cells[row][col]
        cell.classList.toggle(TileMap.toolClasses[2])
        if (row < this.cells.length-1) {
            this.cells[row+1][col].classList.toggle(TileMap.toolClasses[2]+"-above");
            if (col > 0) this.cells[row+1][col-1].classList.toggle(TileMap.toolClasses[2]+"-above-right")
            if (col < this.cells[row+1].length-1) this.cells[row+1][col+1].classList.toggle(TileMap.toolClasses[2]+"-above-left")
        }
    }

    toggleStop(row, col) {
        if (this.cells[row][col].classList.contains(TileMap.toolClasses[0]) || this.cells[row][col].classList.contains(TileMap.toolClasses[1])) return;
        this.removeToolClasses(row, col, 3)

        this.cells[row][col].classList.toggle(TileMap.toolClasses[3]);
        TileMap.additionalToolClasses[3].split(" ").forEach(v => this.cells[row][col].classList.toggle(v))
    }

    toggleWeight(row, col) {
        if (this.cells[row][col].classList.contains(TileMap.toolClasses[0]) || this.cells[row][col].classList.contains(TileMap.toolClasses[1])) return;
        this.removeToolClasses(row, col, 4)

        this.cells[row][col].classList.toggle(TileMap.toolClasses[4]);
        TileMap.additionalToolClasses[4].split(" ").forEach(v => this.cells[row][col].classList.toggle(v))
    }

    setTool(tool) {
        this.tool = tool;
    }
}

export class Tile {

    #score = -1
    #cost = -1
    #future = -1
    #path = 0
    #onPath = false
    #closedSet = false
    #openSet = false
    #previous = null

    constructor(element, attributes={}) {
        this.element = element
        this.tileID = element.getAttribute("row")+","+element.getAttribute("col")
        for (var attr of ["score","travel_cost","estimated_future_cost","onPath","closedSet","openSet","previous"]) if (attr in attributes) this[attr] = attributes[attr]
    }

    clear() {
        for (var attr of ["score","travel_distance","future_distance_estimate","path","closed","open"]) {this.element.removeAttribute(attr);this.element.querySelector(".data").removeAttribute(attr)}
        this.element.removeAttribute("style")
    }

    toString() {
        var type = TileMap.toolClasses.find(c => this.element.classList.contains(c))
        return `cell[${parseInt(this.element.getAttribute("row"))},${parseInt(this.element.getAttribute("col"))}] (${type?type:"empty"})`
    }

    set score(score) {
        this.#score = Math.round(score*100)/100
        this.element.querySelector(".data").setAttribute("score",score.toFixed(1))
    }

    get score() {
        return this.#score
    }

    set travel_cost(cost) {
        this.#cost = Math.round(cost*100)/100
        this.element.querySelector(".data").setAttribute("travel_distance",cost.toFixed(1))
    }

    get travel_cost() {
        return this.#cost
    }

    set path_cost(cost) {
        this.element.querySelector(".data").setAttribute("path_cost",cost.toFixed(1))
        this.#path = Math.round(cost*100)/100
    }

    get path_cost() {
        return this.#path
    }

    set estimated_future_cost(cost) {
        this.#future = Math.round(cost*100)/100
        this.element.querySelector(".data").setAttribute("future_distance_estimate",cost.toFixed(1))
    }

    get estimated_future_cost() {
        return this.#future
    }

    set onPath(onPath) {
        this.#onPath = onPath;
        if (onPath) {
            this.closedSet = false;
            this.openSet = false;
        }
        if (typeof onPath == "string") this.element.style.backgroundColor = onPath
    }

    get onPath() {
        return this.#onPath
    }

    set closedSet(closedSet) {
        this.#closedSet = closedSet;
        if (closedSet) {
            this.onPath = false;
            this.openSet = false;
        }
        this.element.setAttribute("closed",closedSet)
    }

    get closedSet() {
        return this.#closedSet
    }

    set openSet(openSet) {
        this.#openSet = openSet;
        if (openSet) {
            this.onPath = false;
            this.closedSet = false;
        }
        this.element.setAttribute("open",openSet)
    }

    get openSet() {
        return this.#openSet
    }

    set previous(prev) {
        this.#previous = prev
    }

    get previous() {
        return this.#previous
    }
}
import HTML from "../htmlBuilder.js";

/** Holder for all single press buttons in the settings or tools menus */
export default class Actions {
    // Button will randomize map with given randomize function in settings class
    static randomizeActionButtonText = "Randomize Map";
    static randomizeActionButtonClass = "btn-primary";
    static randomizeToastHandler = HTML.basicToast("TileMap Randomized!","success")

    // Button will recreate the initial empty map with origin and target
    static clearActionButtonText = "Clear Map";
    static clearActionButtonClass = "btn-secondary";
    static clearToastHandler = HTML.basicToast("TileMap Cleared!","danger")

    // Button selects tool 'origin' to move and place the origin
    static originActionButtonText = "ORIGIN (Z)";
    static originActionButtonClass = "btn-secondary";
    static originToastHandler = HTML.basicToast("Tool Selected [ORIGIN]","secondary")

    // Button selects tool 'target' to move and place the target
    static targetActionButtonText = "TARGET (X)";
    static targetActionButtonClass = "btn-secondary";
    static targetToastHandler = HTML.basicToast("Tool Selected [TARGET]","secondary")

    // Button selects tool 'wall' which places and toggles wall cells (not replacing origin / target)
    static wallActionButtonText = "WALL (C)";
    static wallActionButtonClass = "btn-secondary";
    static wallToastHandler = HTML.basicToast("Tool Selected [WALL]","secondary")

    // Button selects tool 'stop' which places and toggles stops (not replacing origin / target)
    static stopActionButtonText = "STOP (V)";
    static stopActionButtonClass = "btn-secondary";
    static stopToastHandler = HTML.basicToast("Tool Selected [STOP]","secondary")

    // Button selects tool 'weight' which places and toggles weight cells with higher travel costs (not replacing origin / target)
    static weightActionButtonText = "WEIGHT (B)";
    static weightActionButtonClass = "btn-secondary";
    static weightToastHandler = HTML.basicToast("Tool Selected [WEIGHT]","secondary")

    // Button performs a single step of the currently selected algorithm (chosen in settings class)
    static stepActionButtonText = "STEP (S)";
    static stepActionButtonClass = "btn-success";

    // Key map for performing specific button presses
    shortcuts = {
        "z":this.originActionButtonPressed.bind(this),
        "x":this.targetActionButtonPressed.bind(this),
        "c":this.wallActionButtonPressed.bind(this),
        "v":this.stopActionButtonPressed.bind(this),
        "b":this.weightActionButtonPressed.bind(this),
        "s":this.stepActionButtonPressed.bind(this)
    }

    // Help message displayed at the bottom of the tools menu
    static toolsHelp = "This is some help for tools";

    /**
     * Creates all buttons with appropriate function handlers assigned to each
     * @param {Controller} controller Main controller class used for accessing other aspects of page within button handlers, for example the TileMap class.
     */
    constructor(controller) {
        this.controller = controller

        this.randomizeActionButton = this.createPressButton(this.randomizeActionButtonPressed.bind(this), Actions.randomizeActionButtonText, Actions.randomizeActionButtonClass);
        this.clearActionButton = this.createPressButton(this.clearActionButtonPressed.bind(this), Actions.clearActionButtonText, Actions.clearActionButtonClass);

        this.originActionButton = this.createPressButton(this.originActionButtonPressed.bind(this), Actions.originActionButtonText, Actions.originActionButtonClass);
        this.targetActionButton = this.createPressButton(this.targetActionButtonPressed.bind(this), Actions.targetActionButtonText, Actions.targetActionButtonClass);
        this.wallActionButton = this.createPressButton(this.wallActionButtonPressed.bind(this), Actions.wallActionButtonText, Actions.wallActionButtonClass);
        this.stopActionButton = this.createPressButton(this.stopActionButtonPressed.bind(this), Actions.stopActionButtonText, Actions.stopActionButtonClass);
        this.weightActionButton = this.createPressButton(this.weightActionButtonPressed.bind(this), Actions.weightActionButtonText, Actions.weightActionButtonClass);

        this.stepActionButton = this.createPressButton(this.stepActionButtonPressed.bind(this), Actions.stepActionButtonText, Actions.stepActionButtonClass);

        document.addEventListener("keypress", this.keyPressHandler.bind(this));
    }

    createPressButton(handler, text, thisClass, dismissPanel = true) {
        return HTML.createElement("button",{"onClick":handler},{type:"button",class:"btn "+thisClass, ...(dismissPanel?{"data-bs-dismiss":"offcanvas"}:{})},text)
    }

    /* BUTTON / KEYPRESS handlers */

    keyPressHandler(event) {
        var key = event.key.toLowerCase();
        if (key in this.shortcuts) this.shortcuts[key]();
    }

    randomizeActionButtonPressed() {
        console.log("PATHFINDER ACTION: RANDOMIZE MAP")
        Actions.randomizeToastHandler()
        this.controller.map.randomize(this.controller.settings.randomizer)
    }

    clearActionButtonPressed() {
        console.log("PATHFINDER ACTION: CLEAR MAP")
        Actions.clearToastHandler()
        this.controller.map.createMap()
    }

    originActionButtonPressed() {
        console.log("PATHFINDER ACTION: SELECT ORIGIN TOOL")
        Actions.originToastHandler()
        this.controller.map.setTool(0)
    }

    targetActionButtonPressed() {
        console.log("PATHFINDER ACTION: SELECT TARGET TOOL")
        Actions.targetToastHandler()
        this.controller.map.setTool(1)
    }

    wallActionButtonPressed() {
        console.log("PATHFINDER ACTION: SELECT WALL TOOL")
        Actions.wallToastHandler()
        this.controller.map.setTool(2)
    }

    stopActionButtonPressed() {
        console.log("PATHFINDER ACTION: SELECT STOP TOOL")
        Actions.stopToastHandler()
        this.controller.map.setTool(3)
    }

    weightActionButtonPressed() {
        console.log("PATHFINDER ACTION: SELECT WEIGHT TOOL")
        Actions.weightToastHandler()
        this.controller.map.setTool(4)
    }

    stepActionButtonPressed() {
        console.log("PATHFINDER ACTION: PERFORM STEP")
        if (this.controller.map.changed) {
            this.controller.settings.createAlgorithmInstance();
            this.controller.map.clearDataValues();
            this.controller.map.changed = false;
        }
        this.controller.settings.algorithmInstance.run(false)
    }
}
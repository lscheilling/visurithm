export default class HTML {
    /**
     * Creates a instance of an element with tag, with optional parameters for events, attributes and children
     * @param {string} tag The name of the element
     * @param {Object.<string, any>} settings Add specific events to listen for with the element
     * @param {Object.<string, string>} attributes Add specific attributes for this element
     * @param {...(string | HTMLElement)} children Append children to this element
     */
    static createElement(tag, settings, attributes, ...children) {
        var element = document.createElement(tag);
        for (const attrName in attributes) {
            element.setAttribute(attrName, attributes[attrName])
        }
        if ('onClick' in settings) element.addEventListener("click", settings["onClick"], {passive: true})
        if ('onInput' in settings) element.addEventListener("input", settings["onInput"], {passive: true})
        if ('onChange' in settings) element.addEventListener("change", settings["onChange"], {passive: true})
        if ('mouseDown' in settings) element.addEventListener("mousedown", settings["mouseDown"], {passive: true})
        if ('mouseOver' in settings) element.addEventListener("mouseover", settings["mouseOver"], {passive: true})
        if ('touchStart' in settings) element.addEventListener("touchstart", settings["touchStart"], {passive: true})
        if ('touchMove' in settings) element.addEventListener("touchmove", settings["touchMove"], {passive: true})
        if ('focusLost' in settings) element.addEventListener("focusout", settings["focusLost"], {passive: true})
        element.append(...children);
        return element
    }

    /**
     * Produces a unique ID for a element
     * @returns {string}
     */
    static uid() {
        return Date.now().toString(36) + Math.random().toString(36).substring(2);
    }

    /**
     * Produces a navbar element
     * @param {(string | HTMLElement)} titleElement Element containing the title for the page (with a logo)
     * @param {{backgroundColour: ("primary" | "secondary" | "tertiary" | "body" | "success" | "danger" | "warning" | "info" | "light" | "dark"), darkMode: (false | true), position:(false | "fixed-top" | "fixed-bottom" | "sticky-top" | "sticky-bottom"), noTogglerSize: (false | "sm" | "md" | "lg" | "xl" | "xxl"), useToggler: (false | true)}} settings Settings for the navbar
     * @param {...(string | HTMLElement)} subElements All other sub elements of the navbar
     * @returns {HTMLElement}
     */
    static navbar(titleElement, settings, ...subElements) {
        settings = {
            backgroundColour:"light",
            darkMode: false, 
            position:"fixed-top",
            noTogglerSize:false,
            useToggler:false,
            ...settings
        };
        const uid = this.uid();
        return this.createElement( "nav", {}, {class:`navbar ${settings["darkMode"]?"navbar-dark":""} bg-${settings["backgroundColour"]} ${settings["position"]?settings["position"]:""} ${settings["useToggler"] && settings["noTogglerSize"]?`navbar-expand-${settings["noTogglerSize"]}`:""}`},
            this.createElement("div", {}, {class:"container-fluid"},
                titleElement,
                settings["useToggler"]?this.createElement("button", {}, {
                        class:"navbar-toggler",
                        type:"button", 
                        "data-bs-toggle":"collapse",
                        "data-bs-target":"#"+uid,
                        "aria-controls":uid,
                        "aria-expanded":"false",
                        "aria-label":"Toggle Navbar Navigation"
                    },
                    this.createElement("span",{},{class:"navbar-toggler-icon"})
                ):"",
                settings["useToggler"]?this.createElement("div",{},{class:"collapse navbar-collapse justify-content-end",id:uid},
                    this.list({},...subElements)
                ):this.list({flex:"row"},...subElements)
            )
        )
    }

    /**
     * Returns title element for a navbar
     * @param {string} text Text contained within the title element
     * @param {string=} link Optional link for the element
     * @returns 
     */
    static navTitle(text, link="#") {
        return this.createElement("a", {}, {class:"navbar-brand", href:link}, text)
    }

    /**
     * Returns navbar element that opens an offcanvas
     * @param {string} text The text placed within the navbar link 
     * @param {(string | HTMLElement)} titleElement Element containing the title for the canvas
     * @param {{backgroundColour: ("primary" | "secondary" | "tertiary" | "body" | "success" | "danger" | "warning" | "info" | "light" | "dark" | string), darkMode: (false | true), position:("top" | "bottom" | "start" | "end"), responsive: (false | "sm" | "md" | "lg" | "xl" | "xxl")}} settings Settings for the offcanvas
     * @param {...HTMLElement} subElements All other sub elements of the canvass
     * @returns {HTMLElement}
     */
    static navItemOffcanvas(text, titleElement, settings, ...subElements) {
        var btn = this.createElement("button", {}, {class:"nav-link btn btn-link"}, text)
        return this.createElement("li", {}, {class:"nav-item"},
            this.offcanvas(btn, titleElement, settings, ...subElements)
        )
    }

    /**
     * Produces a offcanvas element
     * @param {(string | HTMLElement)} button Button used to toggle offcanvas element
     * @param {(string | HTMLElement)} titleElement Element containing the title for the canvas
     * @param {{backgroundColour: ("primary" | "secondary" | "tertiary" | "body" | "success" | "danger" | "warning" | "info" | "light" | "dark" | string), darkMode: (false | true), position:("top" | "bottom" | "start" | "end"), responsive: (false | "sm" | "md" | "lg" | "xl" | "xxl")}} settings Settings for the offcanvas
     * @param {...HTMLElement} subElements All other sub elements of the canvas
     * @returns {HTMLElement}
     */
    static offcanvas(button, titleElement, settings, ...subElements) {
        settings = {
            backgroundColour:"light",
            darkMode: false, 
            position:"start",
            responsive:false,
            id:false,
            ...settings
        };
        const uid = settings["id"]?settings["id"]:this.uid();
        titleElement.id = uid+"Label"
        var list = this.list({flex:"column",hover:false,opacity:"100"},...subElements);
        list.style.height="100%";
        return this.createElement("div",{},{},
            this.createElement("div", {}, {
                "data-bs-toggle":"offcanvas",
                "data-bs-target":"#"+uid,
                "aria-controls":uid,
                "aria-expanded":"false",
                "aria-label":"Toggle Offcanvas Navigation"
            }, button),
            this.createElement("div",{},{
                class:`offcanvas offcanvas-${settings["position"]} ${settings["darkMode"]?"text-bg-dark":""} bg-${settings["backgroundColour"]} ${settings["responsive"]?"offcanvas-"+settings["responsive"]:""}`,
                tabindex:"-1",
                id:uid,
                "aria-labelledby":uid+"Label"
            },
                this.createElement("div", {}, {class:"offcanvas-header"},
                    titleElement,
                    this.createElement("button",{},{
                        type:"button",
                        class:`btn-close ${settings["darkMode"]?"btn-close-white":""}`,
                        "data-bs-dismiss":"offcanvas",
                        "aria-label":"Close"
                    })
                ),
                this.createElement("div", {}, {class:"offcanvas-body"}, list)
            )
        )
    }

    /**
     * Returns header for use within offcanvas titleElement
     * @param {string} text Text placed within header
     * @param {number} size Size of the header in the canvas (in range of 1 to 6)
     * @returns {HTMLElement}
     */
    static offcanvasTitle(text, size=5) {
        return this.createElement("h"+size.toString(),{},{class:"offcanvas-title"},text)
    }

    /**
     * Creates a list of elements
     * @param {{colour:("primary" | "secondary" | "tertiary" | "body" | "success" | "danger" | "warning" | "info" | "light" | "dark" | "black" | "white" | string), opacity: ("25" | "50" | "75" | "100"), hover: ("primary" | "secondary" | "success" | "danger" | "warning" | "info" | "light" | "dark" | "black" | "white" | string), bullet: ("none" | "square" | "circle" | "disc" | "decimal" | "lower-alpha" | "upper-alpha" | "lower-roman" | string), margin: ("0" | "1" | "2" | "3" | "4" | "5"), leftPadding: ("0" | "1" | "2" | "3" | "4" | "5"), flex: (false | "row" | "column" | "row-reverse" | "column-reverse")}} settings Settings for the list
     * @param  {...HTMLElement} subElements  All sub elements of the list
     * @returns {HTMLElement}
     */
    static list(settings, ...subElements) {
        settings = {
            colour: "black",
            opacity: "50",
            hover: "black",
            bullet: "none",
            margin: "2",
            leftPadding: "0",
            flex: false,
            ...settings
        }
        return this.createElement("ul", {}, {class:`mb-0 ps-${settings["leftPadding"]} d-flex ${settings["flex"]?"flex-"+settings["flex"]:""} text-${settings["colour"]} ${settings["opacity"]?"text-opacity-"+settings["opacity"]:""}`, style:`list-style:${settings["bullet"]}`},
            ...subElements.map((e,i) => {
                if (i!=0 && (!settings["flex"] || ["row","row-reverse"].includes(settings["flex"]))) e.classList.add(`ms-${settings["margin"]}`);
                if (i!=subElements.length-1 && (!settings["flex"] || ["column","column-reverse"].includes(settings["flex"]))) e.classList.add(`mb-${settings["margin"]}`);
                if (settings["hover"]) e.classList.add("text-hover-"+settings["hover"]);
                return e;
            })
        )
    }

    /**
     * Creates a row element
     * @param {{id: (false | string)}} settings 
     * @param  {...[rowSpacing: (number | [x : number, responsive: (false | "sm" | "md" | "lg" | "xl" | "xxl")][]), element: HTMLElement]} subElements 
     * @returns {HTMLElement}
     */
    static row(settings, ...subElements) {
        settings = {
            id: false,
            ...settings
        }
        var attr = {class:"row"}
        if (settings["id"]) attr["id"] = settings["id"];
        return this.createElement("div",{},attr,
            ...subElements.map(([rowSpacing, element]) => {
                if (typeof rowSpacing == "number") var thisClass = "col-"+rowSpacing.toString();
                else var thisClass = rowSpacing.map((([x, responsive]) => "col-"+responsive+"-"+x.toString())).join(" ");
                return this.createElement("div",{},{class:thisClass},element);
            })
        )
    }

    /**
     * @callback buttonPressCallback runs once when a specified button is pressed
     */
    
    /**
     * Creates a modal popup element with 1 or 2 buttons.
     * @param {string} titleText The main title of the modal popup.
     * @param {HTMLElement | string} bodyElement HTML / Plain text to be placed within the body of the modal. 
     * @param {string} primaryButtonText Text of the main button.
     * @param {String | false | undefined} secondaryButtonText Text of the secondary action button (use false for no button).
     * @param {Object} settings Additional settings for handlers / colour
     * @param {1 | 2 | 3 | 4 | 5} settings.titleSize
     * @param {buttonPressCallback} settings.primaryHandler
     * @param {buttonPressCallback} settings.secondaryHandler
     * @param {"primary" | "secondary" | "success" | "warning" | "danger" | string} settings.primaryColour
     * @param {"primary" | "secondary" | "success" | "warning" | "danger" | string} settings.secondaryColour
     * @param {true | false} settings.staticBackdrop Allows click off with no action required
     * @returns {HTMLElement}
     */
    static modal(titleText, bodyElement, primaryButtonText, secondaryButtonText=false, settings={}) {
        settings = {
            titleSize: 5,
            primaryHandler: false,
            secondaryHandler: false,
            primaryColour: "primary",
            secondaryColour: "secondary",
            staticBackdrop: false,
            ...settings
        }
        return this.createElement("div",{},{class:"modal fade",tabindex:"-1",...(settings["staticBackdrop"]?{"data-bs-backdrop":"static"}:{})},
            this.createElement("div",{},{class:"modal-dialog modal-dialog-centered"},
                this.createElement("div",{},{class:"modal-content"},
                    this.createElement("div",{},{class:"modal-header"},
                        this.createElement("h"+settings["titleSize"].toString(),{},{class:"modal-title"},titleText),
                        this.createElement("button",settings["secondaryHandler"]?{"onClick":settings["secondaryHandler"]}:{},{type:"button",class:"btn-close","data-bs-dismiss":"modal","aria-label":"Close"})
                    ),
                    this.createElement("div",{},{class:"modal-body"}, bodyElement),
                    this.createElement("div",{},{class:"modal-footer"},
                        secondaryButtonText?this.createElement("button",settings["secondaryHandler"]?{"onClick":settings["secondaryHandler"]}:{},{class:"btn btn-"+settings["secondaryColour"],"data-bs-dismiss":"modal"},secondaryButtonText):"",
                        this.createElement("button",settings["primaryHandler"]?{"onClick":settings["primaryHandler"]}:{},{class:"btn btn-"+settings["primaryColour"],"data-bs-dismiss":"modal"},primaryButtonText)
                    )
                )
            )
        )
    }

    /** Single element to hold multiple different toast elements */
    static toastWrapper = this.createElement("div",{},{"aria-live":"polite","aria-atomic":"true",class:"position-absolute top-0 bottom-0 start-0 end-0",style:"pointer-events:none"},
        this.createElement("div",{},{"class":"toast-container bottom-0 end-0 p-3"})
    )

    /** 
     * @callback toastShowCallback run to display the toast notification element.
     */

    /**
     * Creates a toast notification element for use on the page (requires toastWrapper to be placed within page)
     * @param {string} text Text content of the toast notification element.
     * @param {string} colour= Colour of the toast notification element.
     * @returns {toastShowCallback}
     */
    static basicToast(text, colour="secondary") {
        var uid = this.uid()
        var element = this.createElement("div",{},{class:`toast align-items-center text-bg-${colour} border-0`,role:"alert","aria-live":"assertive","aria-atomic":"true",id:uid},
            this.createElement("div",{},{"class":"d-flex"},
                this.createElement("div",{},{class:"toast-body"}, text),
                this.createElement("button",{},{class:`btn-close btn-close-${colour} me-2 m-auto`,"data-bs-dismiss":"toast","aria-label":"Close"})
            )
        )
        HTML.toastWrapper.children[0].appendChild(element)
        return () => {
            const toast = new bootstrap.Toast(document.getElementById(uid))
            toast.show()
        }
    }

    /**
     * Creates footer element for the webpage
     * @param {string} text Text to be placed after copyright
     * @param {*} textColour 
     * @param {*} backgroundColour 
     * @param {*} includeYear 
     * @returns 
     */
    static footer(text, textColour="white", backgroundColour="navy", includeYear = true) {
        return this.createElement("footer",{},{class:`bg-${backgroundColour} text-${textColour} text-center small py-5`},
            this.createElement("div",{},{class:"container"},`Copyright © ${text}${includeYear?" "+new Date().getFullYear().toString():""}`)
        )
    }
}
export class ColourGradient {
    constructor(...splits) {
        this.splits = [[0,"000000ff"],...splits,[1,"ffffffff"]]
    }

    getColour(percentage) {
        if (percentage < 0 || !percentage) return "000000ff"
        else if (percentage > 1) return "ffffffff"
        else {
            var index = this.splits.findIndex(([p1,_],index, arr) => percentage >= p1 && percentage <= arr[index+1][0])
            return this.#split(this.splits[index][1],this.splits[index+1][1], (percentage-this.splits[index][0])/(this.splits[index+1][0]-this.splits[index][0]))
        }
    }

    #int(hex) {
        return parseInt(hex, 16)
    }

    #hex(x) {
        x = x.toString(16);
        return (x.length == 1) ? '0' + x : x;
    }

    #split(colour1, colour2, percentage) {
        var r = Math.round(this.#int(colour1.substring(0,2)) * (1-percentage) + this.#int(colour2.substring(0,2)) * (percentage));
        var g = Math.round(this.#int(colour1.substring(2,4)) * (1-percentage) + this.#int(colour2.substring(2,4)) * (percentage));
        var b = Math.round(this.#int(colour1.substring(4,6)) * (1-percentage) + this.#int(colour2.substring(4,6)) * (percentage));
        var a = Math.round(this.#int(colour1.substring(6,8)) * (1-percentage) + this.#int(colour2.substring(6,8)) * (percentage));
        return this.#hex(r) + this.#hex(g) + this.#hex(b) + this.#hex(a)
    }
}